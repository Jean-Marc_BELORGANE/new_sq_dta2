package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class JPetStoreOutlineSteps {
	
	WebDriver driver;
		
	@Given("We have opened a firefox browser")
	public void we_have_opened_a_firefox_browser() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}
	
	@When("We search for JPetStore Demo Website")
	public void we_search_for_JPetStore_Demo_Website() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@Then("We connecte on JPetStore Demo website")
	public void we_connecte_on_JPetStore_Demo_website() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElement(By.xpath("//div/a[contains(text(),'Sign In')]")).click();
	}

	@When("We log in with our {string} login")
	public void we_log_in_with_our_login(String login) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(login);
	}
	
	@When("We log in with our {string} password")
	public void we_log_in_with_our_password(String password) throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(password);
		Thread.sleep(2000);
		driver.findElement(By.name("signon")).click();
	}

	@Then("We are successfully logged in")
	public void we_are_successfully_logged_in() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//div/a[contains(text(),'Sign Out')]"));
	}

	@Then("The welcome message is dispayed")
	public void the_welcome_message_is_dispayed() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//div[contains(text(),'Welcome ABC!')]"));
		Thread.sleep(4000);
		driver.close();
	}
}