package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class JPetStoreSteps {
	
	WebDriver driver;
	
	@Given("I have opened a firefox browser")
	public void i_have_opened_a_firefox_browser() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}
	
	@When("I search for JPetStore Demo Website")
	public void i_search_for_JPetStore_Demo_Website() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@Then("I connected on JPetStore Demo website")
	public void i_connected_on_JPetStore_Demo_website() {
	    // Write code here that turns the phrase above into concrete actions
	    driver.findElement(By.xpath("//div/a[contains(text(),'Sign In')]")).click();
	}

	@When("I log in with my credentials")
	public void i_log_in_with_my_credentials() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys("j2ee");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("j2ee");
		Thread.sleep(2000);
		driver.findElement(By.name("signon")).click();
	}

	@Then("I am successfully logged in")
	public void i_am_successfully_logged_in() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//div/a[contains(text(),'Sign Out')]"));
	}

	@Then("A welcome message is dispayed")
	public void a_welcome_message_is_dispayed() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//div[contains(text(),'Welcome ABC!')]"));
		//Thread.sleep(4000);
		//driver.close();
	}
}