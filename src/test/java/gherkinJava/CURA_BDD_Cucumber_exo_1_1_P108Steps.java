package gherkinJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CURA_BDD_Cucumber_exo_1_1_P108Steps {
	
	WebDriver driver;

	@Given("L'utilisateur est sur la page d'accueil")
	public void l_utilisateur_est_sur_la_page_d_accueil() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://katalon-demo-cura.herokuapp.com/");
	}

	@When("L'utilisateur souhaite prendre un rendez-vous")
	public void l_utilisateur_souhaite_prendre_un_rendez_vous() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//*[@id=\"btn-make-appointment\"]")).click();
	}

	@Then("La page de connexion s'affiche")
	public void la_page_de_connexion_s_affiche() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("username"));
	}

	@When("L'utilisateur se connecte")
	public void l_utilisateur_se_connecte() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys("John Doe");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("ThisIsNotAPassword");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"btn-login\"]")).click();
	}

	@Then("L'utilisateur est connecte sur la page de rendez-vous")
	public void l_utilisateur_est_connect_sur_la_page_de_rendez_vous() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElement(By.xpath("//*[@id=\"btn-book-appointment\"]"));
		Thread.sleep(2000);
		driver.close();
	}
}
